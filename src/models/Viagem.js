const mongoose = require('mongoose');

const viagemSchema = new mongoose.Schema({
    name: String,
    description: String,
    date_visit: Date,
    photos: [String]
})

const Viagem =  mongoose.model('Viagem', viagemSchema);

module.exports = Viagem;