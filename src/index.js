const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const { MongoMemoryServer } = require('mongodb-memory-server');
const Viagem = require('./models/Viagem')
const multer  = require('multer');


const setup = async() => {
    const mongod = await MongoMemoryServer.create();
    const uri = mongod.getUri();
  
  
    mongoose.connect(`${uri}viagem`, async ()=> {
  
      const app = express();
      const upload = multer({ dest: 'uploads' });
  
      app.use(cors());
      app.use(express.json());
      app.use(express.static('.'))


      app.post('/viagens', upload.array('photos', 20) ,async (req, res) => {
      const { name, description, date_visit } = req.body;
      const photos = req.files.map((file) => {
        return `http://localhost:3001/uploads/${file.filename}`
      });

        const newTrip = await Viagem.create({
            name: name,
            description: description,
            date_visit: date_visit,
            photos: photos
        });

        res.send(newTrip);

      });

      app.get('/viagens', async (req, res)=> {
        const viagens = await Viagem.find({});
        res.send(viagens)
      });

      app.put('/viagens/:id', upload.array('photos', 20),async (req, res) => {
        const viagem = await Viagem.findById(req.params.id);
        const { name, description, date_visit } = req.body;
        const photos = req.files.map((file) => {
          return `http://localhost:3001/uploads/${file.filename}`
        });

        if(viagem){
            viagem.name =  name;
            viagem.description = description;
            viagem.date_visit = date_visit;
            viagem.photos = photos;

            await viagem.save();
            res.send(viagem)

        } else {
            res.status(404);
        }

      });

      app.delete('/viagens/:id', async (req, res) => {
        const viagem = await Viagem.findById(req.params.id);
            await viagem.remove();
            res.status(200).send({message: 'ok apagado'});
      });

      app.listen(3001, () => {
          console.log('Server ON');
      });
  
  }); 
  }
  setup();

